package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net"
	"os/exec"
	"path"
	"strings"
	"time"

	"golang.org/x/crypto/ssh"
)

func getRepoPath(repoPath string) string {
	const repoBasePath = "/home/mark/Projects/go/src/bitbucket.org/markadams/"
	repoPath = strings.Replace(repoPath, "'", "", -1)

	newPath := path.Join(repoBasePath, repoPath)

	log.Println(newPath)

	if !path.IsAbs(newPath) {
		return ""
	}

	return newPath

}

func communicate(in io.Reader, out io.Writer, id string) {
	io.Copy(out, in)
}

func handleConn(conn *net.Conn, config *ssh.ServerConfig) {
	// Before use, a handshake must be performed on the incoming
	// net.Conn.
	_, chans, reqs, err := ssh.NewServerConn(*conn, config)

	if err != nil {
		panic("failed to handshake")
	}
	// The incoming Request channel must be serviced.
	go ssh.DiscardRequests(reqs)

	// Service the incoming Channel channel.
	for newChannel := range chans {
		// Channels have a type, depending on the application level
		// protocol intended. In the case of a shell, the type is
		// "session" and ServerShell may be used to present a simple
		// terminal interface.
		if newChannel.ChannelType() != "session" {
			newChannel.Reject(ssh.UnknownChannelType, "unknown channel type")
			continue
		}
		channel, requests, err := newChannel.Accept()
		if err != nil {
			panic("could not accept channel.")
		}

		// Sessions have out-of-band requests such as "shell",
		// "pty-req" and "env".  Here we handle only the
		// "shell" request.
		commands := make(chan string, 1)
		timeout := make(chan bool, 1)

		go func(in <-chan *ssh.Request, out chan<- string) {
			for req := range in {
				ok := false
				switch req.Type {
				case "exec":
					ok = true
					execCmd := string(req.Payload[4:])
					out <- execCmd
				}
				log.Printf("type: %s", req.Type)
				log.Printf("payload: %s", string(req.Payload))
				req.Reply(ok, nil)
			}
			log.Printf("Done with requests")
		}(requests, commands)

		go func(timeout chan<- bool) {
			time.Sleep(1 * time.Second)
			timeout <- true
		}(timeout)

		var commandStr string

		select {
		case commandStr = <-commands:
			// Some stuff happens later
		case <-timeout:
			panic("Timeout!")
		}

		command := strings.Split(commandStr, " ")
		repoPath := getRepoPath(command[1])

		log.Printf("%v", command)
		cmd := exec.Command(command[0], repoPath)
		log.Printf("%v", command[1])

		inPipe, err := cmd.StdinPipe()

		if err != nil {
			panic(err)
		}

		outPipe, err := cmd.StdoutPipe()

		if err != nil {
			panic(err)
		}

		errPipe, err := cmd.StderrPipe()

		if err != nil {
			panic(err)
		}

		go communicate(channel, inPipe, "ssh-to-proc")
		go communicate(outPipe, channel, "proc-to-ssh")
		go communicate(errPipe, channel.Stderr(), "proc-err-to-ssh")

		err = cmd.Start()

		if err != nil {
			panic(err)
		}

		err = cmd.Wait()

		channel.SendRequest("exit-status", false, []byte{0, 0, 0, 0})
		log.Printf("%v", cmd.ProcessState.Success())

		if err != nil {
			panic(err)
		}

		channel.Close()

		log.Println("Done!")
	}
}

func main() {

	config := &ssh.ServerConfig{
		PublicKeyCallback: func(conn ssh.ConnMetadata, key ssh.PublicKey) (*ssh.Permissions, error) {
			return nil, nil
		},
		PasswordCallback: func(c ssh.ConnMetadata, pass []byte) (*ssh.Permissions, error) {
			// Should use constant-time compare (or better, salt+hash) in
			// a production setting.
			if c.User() == "testuser" && string(pass) == "tiger" {
				return nil, nil
			}
			return nil, fmt.Errorf("password rejected for %q", c.User())
		},
	}

	privateBytes, err := ioutil.ReadFile("id_rsa")
	if err != nil {
		panic("Failed to load private key")
	}

	private, err := ssh.ParsePrivateKey(privateBytes)
	if err != nil {
		panic("Failed to parse private key")
	}

	config.AddHostKey(private)

	// Once a ServerConfig has been configured, connections can be
	// accepted.
	listener, err := net.Listen("tcp", "0.0.0.0:2022")
	if err != nil {
		panic("failed to listen for connection")
	}

	for {
		nConn, err := listener.Accept()
		if err != nil {
			panic("failed to accept incoming connection")
		}

		go handleConn(&nConn, config)
	}

}
